require.config ({
	baseUrl: 'app',
	paths: {
		/* Modules */
		jquery: '../node_modules/jquery/dist/jquery',
		underscore: '../node_modules/underscore/underscore',
		backbone: '../node_modules/backbone/backbone',

		/* Models */
		model: 'js/models/model',
		celsiusModel: 'js/models/celsiusModel',

		/* Views */
		alertView:              'js/views/alertView',
		conditionsView:         'js/views/conditionsView',
		conditionsCelsiusView:  'js/views/conditionsCelsiusView',
		forecastView: 	        'js/views/forecastView',
		forecastCelsiusView: 	'js/views/forecastCelsiusView',
		hourlyView: 	        'js/views/hourlyView',
		hourlyCelsiusView: 	    'js/views/hourlyCelsiusView',
		mapView: 	            'js/views/mapView',
		searchView: 	        'js/views/searchView',
		collectionView:         'js/views/collectionView',
		collectionCelsiusView:  'js/views/collectionCelsiusView',

		/* Collections */
		collectionBase: 'js/collections/collectionBase',
		collection: 'js/collections/collection',

		/* Routes */
		router: 'js/routes/router'
	},
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		}
	}
});

define(['collectionView', 'collection', 'router'], 
    function(view, collection, router) {
        new router();
        Backbone.history.start();
        new view ({
            collection: new collection,
        });
    });
