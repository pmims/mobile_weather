define(['jquery', 'underscore', 'backbone', 'conditionsCelsiusView', 'forecastCelsiusView', 'hourlyCelsiusView', 'collection'], 
	function($, _, Backbone, conditionsCelsiusView, forecastCelsiusView, hourlyCelsiusView, collection) {
		var collectionCelsiusView = Backbone.View.extend({
			initialize: function() {
				console.log("collectionView");
				this.listenTo(this.collection, "reset", this.render, this);
				this.fetch();
			},

			fetch: function() {
				this.collection.fetch({reset: true});
				this.render();
			},

			render: function() {
				this.collection.each(function(item) {
					this.renderModel(item);
				}, this);
			},

			renderModel: function(item) {
				new conditionsCelsiusView ({
					model: item
				});
				new forecastCelsiusView ({
					model: item
				});
				new hourlyCelsiusView ({
					model: item
				});
			}
		});
		return collectionCelsiusView;
	});
