define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var conditionsCelsiusView = Backbone.View.extend({
		initialize: function() {
            this.listenTo(this.model, 'change', this.render);
            this.model.fetch();
		},

        el: "#conditions",

		template: _.template($("#conditions-celsius-template").html()),

		render: function() {
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return conditionsCelsiusView;
});
