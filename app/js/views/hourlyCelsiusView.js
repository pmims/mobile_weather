define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var hourlyCelsiusView = Backbone.View.extend({
		initialize: function() {
            console.log("hourlyCelsiusView");
            this.listenTo(this.model, 'change', this.render);
            this.model.fetch();
		},

        el: "#hourly",

		template: _.template($("#hourly-celsius-template").html()),

		render: function() {
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return hourlyCelsiusView;
});
