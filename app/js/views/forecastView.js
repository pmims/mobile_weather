define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var forecastView = Backbone.View.extend({

		initialize: function() {
			console.log("forecastView");
			this.render();
		},

		el: "#forecast",

		events: {
			'click .day_of_week_0' : 'showDayZero',
			'click .day_of_week_1' : 'showDayOne',
			'click .day_of_week_2' : 'showDayTwo',
			'click .day_of_week_3' : 'showDayThree',
			'click .day_of_week_4' : 'showDayFour',
			'click .day_of_week_5' : 'showDayFive'
		},

		showDayZero: function() {
			console.log("This is day zero...");
		},

		showDayOne: function() {
			console.log("This is day one...");
		},

		showDayTwo: function() {
			console.log("This is day two...");
		},

		showDayThree: function() {
			console.log("This is day three...");
		},

		showDayFour: function() {
			console.log("This is day four...");
		},

		showDayFive: function() {
			console.log("This is day five...");
		},

		template: _.template($("#forecast-template").html()),

		render: function() {
			console.log(this.model.toJSON());
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return forecastView;
});
