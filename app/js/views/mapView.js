define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var mapView = Backbone.View.extend({
		initialize: function() {
			this.render();
            this.mapbox();
		},

        el: "#map",

        mapbox: function() {
            const response = _.clone(this.model.get("response"));
            const coords = Object.assign({}, response.current_observation.display_location);

            mapboxgl.accessToken = 'api_key';
            let map = new mapboxgl.Map({
                container: 'map_area',
                style: 'mapbox://styles/mapbox/streets-v10',
                center: [coords.longitude, coords.latitude],
                zoom: 13
            });
        },

		template: _.template($("#map-template").html()),

		render: function() {
			return this.$el.html("<div id='map_area' style='height: 200px; width: 100%;'></div>");
		}
	});
	return mapView;
});
