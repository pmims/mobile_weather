define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var forecastCelsiusView = Backbone.View.extend({
		initialize: function() {
            console.log("forecastCelsiusView");
            this.listenTo(this.model, 'change', this.render);
            this.model.fetch();
            console.log("ForecastCelsiusView", this.model.toJSON())
		},

        el: "#forecast",

		template: _.template($("#forecast-celsius-template").html()),

		render: function() {
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return forecastCelsiusView;
});
