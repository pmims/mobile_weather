define(['jquery', 'underscore', 'backbone', 'alertView', 'conditionsView', 'forecastView', 'mapView', 'hourlyView', 'searchView', 'collection'], 
	function($, _, Backbone, alertView, conditionsView, forecastView, mapView, hourlyView, searchView, collection) {
		var collectionView = Backbone.View.extend({
			initialize: function() {
				console.log("collectionView");
				this.listenTo(this.collection, "reset", this.render, this);
				this.fetch();
			},

			fetch: function() {
				this.collection.fetch({reset: true});
				this.render();
			},

			render: function() {
				this.collection.each(function(item) {
					this.renderModel(item);
				}, this);
			},

			renderModel: function(item) {
				new searchView ({
					model: item
				});
				new alertView ({
					model: item
				});
				new conditionsView ({
					model: item
				});
				new mapView ({
					model: item
				});
				new forecastView ({
					model: item
				});
				new hourlyView ({
					model: item
				});
			}
		});
		return collectionView;
	});
