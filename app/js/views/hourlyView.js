define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var hourlyView = Backbone.View.extend({
		initialize: function() {
			console.log("hourlyView");
			this.render();
		},

		el: "#hourly",

		template: _.template($("#hourly-template").html()),

		render: function() {
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return hourlyView;
});
