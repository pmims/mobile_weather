define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var searchView = Backbone.View.extend({
		initialize: function() {
			this.render();
		},

		el: "#search",

        events: {
            'click #cog': 'testing'
        },

        testing: function() {
            console.log("hello cog!");
        },

		template: _.template($("#search-template").html()),

		render: function() {
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return searchView;
});
