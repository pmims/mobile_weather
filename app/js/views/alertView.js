define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
    var alertView = Backbone.View.extend({
        initialize: function() {
            console.log("alertView initialized");
            this.render();
        },
        
        el: "#alert",

		template: _.template($("#alert-template").html()),

		render: function() {
			console.log("ALERTS HERE: " + this.model.toJSON());
			return this.$el.html(this.template(this.model.toJSON()));
		}
    });

    return alertView;
});
