define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var conditionsView = Backbone.View.extend({
		initialize: function() {
			console.log("conditionsView");
			this.render();
		},

        el: "#conditions",

		template: _.template($("#conditions-template").html()),

		render: function() {
			console.log(this.model.toJSON());
			return this.$el.html(this.template(this.model.toJSON()));
		}
	});
	return conditionsView;
});
