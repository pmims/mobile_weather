define(['jquery', 'underscore', 'backbone'], 
	function($, _, Backbone, routeModel) {
		var collectionBase = Backbone.Collection.extend({
			initialize: function() {
				console.log("collectionBase.js init.");
			},
			parse: function(response) {
				if(_.isObject(response)) {
					return {
						"response": response
					}
				}
			}
		});
		return collectionBase;
	});
