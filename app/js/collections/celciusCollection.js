define(['jquery', 'underscore', 'backbone', 'model', 'collectionBase'], 
	function($, _, Backbone, model, collectionBase) {

		var collection = collectionBase.extend({
			initialize: function() {
				console.log("collection");
			},

			model: model,

			url: 'http://api.wunderground.com/api/f4de0f94a19a7665/conditions/forecast10day/geolookup/hourly10day/q/wa/seattle.json'
		});
		return collection;
	});
