define(['jquery', 'underscore', 'backbone', 'model', 'collectionBase'], 
	function($, _, Backbone, model, collectionBase) {

		var collection = collectionBase.extend({
			initialize: function() {
				console.log("collection");
			},

			model: model,
                
            url: 'api_key'

		});
		return collection;
	});
