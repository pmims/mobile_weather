define(['jquery', 'underscore', 'backbone', 'collectionView', 'collectionCelsiusView', 'collection'],
    function($, _, Backbone,  collectionView, collectionCelsiusView, collection) {
        var router = Backbone.Router.extend({
            initialize: function() {
                console.log("router", collection);
            },
            routes: {
                "fahrenheit": "fahrenheitFCT",
                "celsius": "celsiusFCT"
            },
            fahrenheitFCT: function() {
                new collectionView ({
                    collection: new collection 
                });
            },

            celsiusFCT: function() {
                new collectionCelsiusView ({
                    collection: new collection 
                });
            }
        });
        return router;
    });
