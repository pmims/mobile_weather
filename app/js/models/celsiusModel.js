define(['backbone'], function(Backbone) {
    const model = Backbone.Model.extend({
        initialize: function() {
            console.log("celsius model");
        },
        url: 'http://api.wunderground.com/api/f4de0f94a19a7665/conditions/forecast10day/geolookup/hourly10day/q/wa/seattle.json'
    });
    return model;
});
